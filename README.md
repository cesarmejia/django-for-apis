<!-- TOC orderedList:true anchorMode:gitlab.com -->

# Django for APIs

Code along repo to William Vincent's book by same title

## Table of Contents

- [Introduction](#introduction)
- [Chapter 01: Web APIs](#chapter-01-web-apis)
- [Chapter 02: Library Website and API](#chapter-02-library-website-and-api)
- [Chapter 03: Todo API](#chapter-03-todo-api)
- [Chapter 04: Todo React Front-end](#chapter-04-todo-react-front-end)
- [Chapter 05: Blog API](#chapter-05-blog-api)
- [Chapter 06: Permissions](#chapter-06-permissions)
- [Chapter 07: User Authentication](#chapter-07-user-authentication)
- [Chapter 08: Viewsets and Routers](#chapter-08-viewsets-and-routers)
- [Chapter 09: Schemas and Documentation](#chapter-09-schemas-and-documentation)
- [Conclusion](#conclusion)

## Completed

- [x] ~~_Introduction_~~ [2019-12-27]
- [x] ~~_Chapter 01_~~ [2020-01-01]
- [x] ~~_Chapter 02_~~ [2020-01-01]
- [x] ~~_Chapter 03_~~ [2020-01-01]
- [x] ~~_Chapter 04_~~ [2020-01-01]
- [x] ~~_Chapter 05_~~ [2020-01-01]
- [x] ~~_Chapter 06_~~ [2020-01-01]
- [x] ~~_Chapter 07_~~ [2020-01-01]
- [x] ~~_Chapter 08_~~ [2020-01-01]
- [x] ~~_Chapter 09_~~ [2020-01-01]
- [x] ~~_Conclusion_~~ [2020-01-01]

  <!-- /TOC -->

## Introduction

- _The internet is powered by RESTful APIs_

- `API` _Application Programming Interface_

- Web APIs overwhelmingly structured in a `RESTful pattern` _REpresentational State Transfer_

- We will build several web APIs from scratch using Django and Django REST Framework `DRF`

- _Django's "batteries-included" approach masks much of the underlying complexity and security risks involved in creating any web API._

- _Truly mastering Python takes years_ ... shit ... 😅

- Django first released in 2005

- Recently `API-first` approach has emerged as arguably the dominant paradigm in web development

- _This approach of dividing services into different components, by the way, is broadly known as [Service-oriented architecture](https://en.wikipedia.org/wiki/Service-oriented_architecture)_.

- Advantages to Service-oriented architecture:

  1. More "future-proof" (i.e. _any_ front-end can consume backend); relevant considering fast-pace of front-end framework development (React released in 2013; Vue in 2014)
  2. API-first back-end can support several front-ends (React, React Native, Mobile Native libraries)
  3. API-first can be used both internally and externally (other developers can make front-end apps; internal team may not need to dedicate development time)

- Quizlet is now a top 20 website in the U.S. during the school year.

  - ??? What other education tech space apps are prime for development?

- `Django REST Framework` is arguably **the** killer app for Django.
  - Pros: mature, full of features, customizable, testable, extremely well-documented

## Chapter 01: Web APIs

- Web APIs rely on standards: `HTTP`, `IP/TCP`, and others

- Review basic terms: `endpoints`, `resources`, `HTTP verbs`, `HTTP status codes`, and `REST`

- _The Internet is a system of interconnected computer networks_

- 1989, **Tim Berners-Lee** invents `HTTP`, [Hypertext Transfer Protocol](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol)

  - Read Wiki when time permits

- `URL`, Uniform Resource Locator

- `DNS`, Domain Name Service: translates _domain name_ into _IP address_

- `TCP`, Transmission Control Protocol

- `API endpoints`, URLs + HTTP Verb

- _**HTTP** is a **request-response** protocol between two computers that have an existing TCP connection._

- _HTTP is a **stateless** protocol_ (i.e. lacks memory)

- Every HTTP message consists of 3 parts:

  1. request/[status](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes) line
  2. [headers](https://en.wikipedia.org/wiki/List_of_HTTP_header_fields)
  3. optional body data

- `REST`, REpresentational State Transfer

  - First proposed in 2000 by Roy Fielding

- Every RESTful API (rule of thumb):

  1. is stateless, like HTTP
  2. supports common HTTP verbs (GET, POST, PUT, DELETE, etc.)
  3. returns data in either JSON of XML format

- _Ultimately a web API is a collection of endpoints that expose certain parts of an underlying database_

## Chapter 02: Library Website and API

- _Django REST Framework works alongside the Django web framework to create web APIs._

- `pip install djangorestframework==3.9.2` installs DRF

  - Then add `rest_framework` to `INSTALLED APPS` in `settings.py`

- `python manage.py startapp api` create api app

  - Then add `api.apps.ApiConfig` to `INSTALLED APPS` in `settings.py`

- In `api/views.py`:

  - `from rest_framework.generics import ListAPIView`
  - `from .serializers import BookSerializer`

  ```python
  class BookAPIView(ListAPIView):
  queryset = Book.objects.all()
  serializer_class = BookSerializer
  ```

- _A **serializer** translates data into a format that is easy to consume over the internet, typically JSON, and is displayed at an endpoint._

  - In `api/serializers.py`:

    - `from rest_framework import serializers`

  ```python
  class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = ('title', 'subtitle', 'author', 'isbn')
  ```

- Django REST Framework (DRF) automatically serves endpoint resources in `format=api`. It's hawt!

## Chapter 03: Todo API

- _The only real difference between Django REST Framework and Django is that with DRF we need to add a `serializers.py` file and we do not need a templates file._

- Setting up `CORS`, Cross-Origin-Resource-Sharing

  - `pip install django-cors-headers`
  - In `settings.py`, - Add `corsheaders` to `INSTALLED_APPS` - Add `corsheaders.middleware.CorsMiddleware` and `django.middleware.common.CommonMiddleware` to TOP of `MIDDLEWARE` - Add `CORS_ORIGIN_WHITELIST` tuple

  ```python
  CORS_ORIGIN_WHITELIST = [
  'localhost:3000',
  ]
  ```

- `CORS_ORIGIN_ALLOW_ALL = True`, whitelist all origins

- Server now expects `Origin` header

```python
CORS_ALLOW_HEADERS = [
    'accept',
    'accept-encoding',
    'authorization',
    'content-type',
    'dnt',
    'origin',
    'user-agent',
    'x-csrftoken',
    'x-requested-with',
]
```

- **OR to extend beyond default headers**

```python
CORS_ALLOW_HEADERS = list(default_headers) + [
    'my-custom-header',
]
```

- `CORS_ALLOW_CREDENTIALS = True`, allow cookies to be included in cross-site HTTP requests

- `CSRF_TRUSTED_ORIGINS = [ 'change.allowed.com', ]`, ??? something to do with CSRF Token

## Chapter 04: Todo React Front-end

- Nothing fancy. Used React front-end and the axios library to populate component state with a `ListAPIView` from back-end.

## Chapter 05: Blog API

- Django auth User: `django.contrib.auth.models.User`

- Adding a created_at field: `created_at = models.DateTimeField(auto_now_add=True)`

- Adding an updated_at field: `updated_at = models.DateTimeField(auto_now=True)`

- New API generics: `generics.ListCreateAPIView` and `genericsRetrieveUpdateDestroyAPIView`

- Brief chapter. Mainly introduces new powerful views that allow for List/Post on collections and Create-Update-Delete on model instances.

## Chapter 06: Permissions

- _Django REST Framework ships with several out-of-the-box permissions settings that we can use to secure our API. These can be applied at a project-level, a view-level, or at any individual model level._

- _It is a much simpler and safer approach to set a strict permissions policy at the project-level and loosen it as needed at the view level._

- Project-level permissions:

  - `AllowAny` - any user has full access
  - `IsAuthenticated` - only authenticated users have access
  - `IsAdminUser` - only admins/superusers have access
  - `IsAuthenticatedOrReadOnly` - unauthorized users can view any page, but only authenticated users have write, edit, or delete privileges

- `permission_classes` be careful with spelling; will fail silently (e.g. `permissions_classes`)

- Add `CustomPermissions` in `permissions.py`:

```python
from rest_framework import permissions


class IsAuthorOrReadOnly(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        # Read-only permissions are allowed for any request
        if request.method in permissions.SAFE_METHODS:
            return True
        # Write permissions are only allowed to the author of a post
        return obj.author == request.user
```

## Chapter 07: User Authentication

- "Basic" Authentication:

  - `username:password` passed in `Authorization` header as base64 encoded string

- Session Authentication:

  - After authenticating user once via username:password, the server passes a `session_id` to client and stores a `session_object` that has user details. Client will send session_id in a cookie header on all future request. Session_id and session_object are destroyed on logout. **stateful** scheme

- _The default setting in Django REST Framework is a combination of Basic and Session Authentication._

- _It is generally not advised to use a session-based authentication scheme for any API that will have multiple front-ends_

- Token Authentication:

  - **stateless** scheme
  - Token contains the user details

- Default permission settins is `AllowAny`
- Default authentication settings are `Basic` + `Session`

```python
REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAuthenticated',
    ],
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.BasicAuthentication'
        # 'rest_framework.authentication.TokenAuthentication' # if using token auth
    ]
}
```

- To use Token Authentication, must edit `settings.py` and add to `INSTALLED_APPS`, `rest_framework.authtoken`

- `django-rest-auth` Adds log-in, log-out and password_reset/change routes

  - In `settings.py`, add `rest_auth` to `INSTALLED_APPS` and in project-urls, add `path('api/v1/rest-auth', include('rest_auth.urls'))`

- `django-allauth` Adds local and social registration flow

  - Add to `INSTALLED_APPS`,
    - `django.contrib.sites`
    - `allauth`
    - `allauth.account`
    - `allauth.socialaccount`
    - `rest_auth.registration`
  - Also add `EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'` and `SITE_ID` to `settings.py`
  - Finally, to project-urls: `path('api/v1/rest-auth/registration/, include('rest_auth.registration.urls'))`

- NOTE: This chapter felt heavy on the config and light on the explanation.

## Chapter 08: Viewsets and Routers

- New endpoints _always_ involves the following three steps:

  1. New serializer class for the model
  2. New views for **each** endpoint
  3. New URL routes for **each** endpoint

- _A **viewset** is a way to combine the logic for multiple related views into a single class._

- _**Routers** work directly with viewsets to automatically generate URL patterns for us._

- Another unsatisfactory chapter. You are told which buttons to press, but motivation is lacking.

- **Viewset Refactor**:

```python
from django.contrib.auth import get_user_model
from rest_framework import viewsets

from .models import Post
from .permissions import IsAuthorOrReadOnly
from .serializers import PostSerializer, UserSerializer


class PostViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthorOrReadOnly,)
    queryset = Post.objects.all()
    serializer_class = PostSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer
```

- **Router Refactor**

```python
from django.urls import path
from .views import UserViewSet, PostViewSet
from rest_framework.routers import SimpleRouter

router = SimpleRouter()
router.register('users', UserViewSet, base_name='users')
router.register('', PostViewSet, base_name='posts')

urlpatterns = router.urls
```

## Chapter 09: Schemas and Documentation

- _A **schema** is a machine-readable document that outlines all available API endpoints_

- _By the end we will have implemented an automated way to document any current and future changes to our API._

- Quick introduction on how to add automated documentation to API:

```python
from django.contrib import admin
from django.urls import path, include
from rest_framework.documentation import include_docs_urls
from rest_framework.schemas import get_schema_view
from rest_framework_swagger.views import get_swagger_view

API_TITLE = 'Blog API'
API_DESCRIPTION = 'A Web API for creating and editing blog posts.'
schema_view = get_swagger_view(title=API_TITLE)

urlpatterns = [
path('admin/', admin.site.urls),
path('api/v1/', include('posts.urls')),
path('api-auth/', include('rest_framework.urls')),
path('api/v1/rest-auth/', include('rest_auth.urls')),
path('api/v1/rest-auth/registration/',
include('rest_auth.registration.urls')),
path('docs/', include_docs_urls(title=API_TITLE,
description=API_DESCRIPTION)), # path('schema/', schema_view),
path('swagger-docs/', schema_view)
```

```python
SWAGGER_SETTINGS = {
    'LOGIN_URL': 'rest_framework:login',
    'LOGOUT_URL': 'rest_framework:logout',
}
```

## Conclusion

- [Official DRF docs](https://www.django-rest-framework.org/)

- [Official Tutorial Walkthrough by Vincent](https://wsvincent.com/official-django-rest-framework-tutorial-beginners-guide/)

- [Awesome-Django Packages](https://github.com/wsvincent/awesome-django)

- Django REST Framework, initially created by **Tom Christie**
