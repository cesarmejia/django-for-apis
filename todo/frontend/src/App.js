import React, { Component } from 'react';
import axios from 'axios';
const list = [
  // {
  //   body: 'Complete all 9 chapters by 5pm tonight!',
  //   id: 1,
  //   title: 'Learn Django API',
  // },
  // {
  //   body: 'Watch the shortest intro to cypress.io and get some boilerplate test implemented on the front-end.',
  //   id: 2,
  //   title: 'Learn Cypress.io',
  // },
  // {
  //   body: 'Get a handle on TypeScript + ReduxToolkit code structure',
  //   id: 3,
  //   title: 'Read more TypeScript + ReduxToolkit',
  // },
];

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = { list };
  }

  componentDidMount() {
    axios
      .get('http://localhost:8080/api/')
      .then(res => this.setState({ list: res.data }))
      .catch(err => console.error(err));
  }

  render() {
    return (
      <div>
        {this.state.list.map(item => (
          <div key={item.id}>
            <h1>{item.title}</h1>
            <p>{item.body}</p>
          </div>
        ))}
      </div>
    );
  }
}
